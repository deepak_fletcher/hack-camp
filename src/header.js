import React, { useState } from 'react';
import {

  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './header.css';
const Example = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div style={{fontWeight:'bold'}}>
      <Navbar color="light" light expand="md">
      <NavbarBrand href="/">All Thing Store</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
         

           <Nav className="mr-auto" navbar>

            
            <NavItem>
              <NavLink href="/login">Login</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                MyAccount
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  OrderHistory
                </DropdownItem>
                <DropdownItem>
                  Transcation
                </DropdownItem>

              </DropdownMenu>
            </UncontrolledDropdown>
            <NavbarText>Please Register or Login</NavbarText>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Example;
