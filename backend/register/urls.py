from django.urls import path
from . import views

urlpatterns = [
    path('register/register/', views.RegisterListCreate.as_view() ),
]