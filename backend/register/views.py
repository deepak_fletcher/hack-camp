from .models import Register
from .serializers import RegisterSerializer
from rest_framework import generics

class RegisterListCreate(generics.ListCreateAPIView):
    queryset = Register.objects.all()
    serializer_class = RegisterSerializer