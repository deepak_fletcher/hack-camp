from django.db import models

# Create your models here.


class Api(models.Model):
    name = models.CharField(max_length=100)
    item = models.CharField(max_length=100)
    price = models.FloatField(max_length=100)