from django.urls import path
from . import views

urlpatterns = [
    path('api/api/', views.ApiListCreate.as_view() ),
]