from .models import Api
from .serializers import ApiSerializer
from rest_framework import generics

class ApiListCreate(generics.ListCreateAPIView):
    queryset = Api.objects.all()
    serializer_class = ApiSerializer